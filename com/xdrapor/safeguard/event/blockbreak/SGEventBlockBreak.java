package com.xdrapor.safeguard.event.blockbreak;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;

import com.xdrapor.safeguard.checks.SGCheck;
import com.xdrapor.safeguard.checks.blockbreak.SGCheckReach;
import com.xdrapor.safeguard.checks.blockbreak.SGCheckSpeed;
import com.xdrapor.safeguard.event.SGEventListener;

public class SGEventBlockBreak extends SGEventListener 
{

	@EventHandler(priority = EventPriority.LOWEST)
	public void checkBlockDamage(BlockDamageEvent playerDamage) {
		// This event does not produce a check, it only sets the data for the SGPlayer instance.
		safeGuard.sgPlayerManager.getPlayer(playerDamage.getPlayer().getName()).setLastBlockHitTime(System.currentTimeMillis());
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void checkBlockBreak(BlockBreakEvent playerBreak)
	{
		// This event produces both data for a check and executes the actual checks.
		
		safeGuard.sgPlayerManager.getPlayer(playerBreak.getPlayer().getName()).setLastBlockBreakTime(System.currentTimeMillis());
		
		for(SGCheck sgCheck : sgChecks)
		{
			sgCheck.runCheck(playerBreak, safeGuard.sgPlayerManager.getPlayer(playerBreak.getPlayer().getName()));
		}
	}
	
	@Override
	public void loadChecks()
	{
		sgChecks.add(new SGCheckReach());
		sgChecks.add(new SGCheckSpeed());
	}
}
