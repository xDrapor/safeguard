package com.xdrapor.safeguard.event.combat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.xdrapor.safeguard.checks.SGCheck;
import com.xdrapor.safeguard.checks.combat.SGCheckSpeed;
import com.xdrapor.safeguard.event.SGEventListener;

public class SGEventCombat extends SGEventListener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void checkEntityAttack(EntityDamageByEntityEvent entityAttack)
	{
		for(SGCheck sgCheck : sgChecks)
		{
			if(entityAttack.getDamager() instanceof Player)
			{
				sgCheck.runCheck(entityAttack, safeGuard.sgPlayerManager.getPlayer(((Player)entityAttack.getDamager()).getName()));
			}
		}
	}
	
	@Override
	public void loadChecks() 
	{
		sgChecks.add(new SGCheckSpeed());
	}

}
