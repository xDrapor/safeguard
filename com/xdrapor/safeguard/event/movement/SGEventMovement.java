package com.xdrapor.safeguard.event.movement;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import com.xdrapor.safeguard.checks.SGCheck;
import com.xdrapor.safeguard.checks.movement.SGCheckFlight;
import com.xdrapor.safeguard.checks.movement.SGCheckInvalidMove;
import com.xdrapor.safeguard.checks.movement.SGCheckSpeed;
import com.xdrapor.safeguard.event.SGEventListener;
import com.xdrapor.safeguard.utilities.SGMovementUtil;

public class SGEventMovement extends SGEventListener
{
	@EventHandler(priority = EventPriority.LOWEST)
	public void checkPlayerMovement(PlayerMoveEvent playerMove) 
	{
		for(SGCheck sgCheck : sgChecks)
		{
			sgCheck.runCheck(playerMove, safeGuard.sgPlayerManager.getPlayer(playerMove.getPlayer().getName()));
		}
	}


	@EventHandler(priority = EventPriority.LOWEST)
	public void checkPlayerRespawn(PlayerRespawnEvent playerRespawn)
	{
		// SGMovementData sgPlayerMovementData = new SGMovementData(playerRespawn); NO LONGER APPLICABLE @see SGCheckFlight for example.
		// This event does not produce a check, it only sets the data for the SGPlayer instance.
		SGMovementUtil.setSafeLocationSpawn(playerRespawn.getPlayer());
	}

	
	@EventHandler(priority = EventPriority.LOWEST)
	public void checkPlayerTeleport(PlayerTeleportEvent playerTeleport)
	{
		// SGMovementData sgPlayerMovementData = new SGMovementData(playerTeleport); NO LONGER APPLICABLE @see SGCheckFlight for example.
		// This event does not produce a check, it only sets the data for the SGPlayer instance.
		if(playerTeleport.getCause().equals(TeleportCause.UNKNOWN))
		{
			if(playerTeleport.getFrom().getBlock().getType().equals(Material.BED_BLOCK)) {
				safeGuard.sgPlayerManager.getPlayer(playerTeleport.getPlayer().getName()).resetSafeLocation();
			}
			
			return;
		}
		
		safeGuard.sgPlayerManager.getPlayer(playerTeleport.getPlayer().getName()).resetSafeLocation();
	}

	@Override
	public void loadChecks() 
	{
		sgChecks.add(new SGCheckFlight());
		sgChecks.add(new SGCheckSpeed());
		sgChecks.add(new SGCheckInvalidMove());
	}
}
