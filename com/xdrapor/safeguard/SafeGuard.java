package com.xdrapor.safeguard;

import org.bukkit.plugin.java.JavaPlugin;

import com.xdrapor.safeguard.core.ICore;
import com.xdrapor.safeguard.core.configuration.SGConfig;
import com.xdrapor.safeguard.core.loading.SGLoadManager;
import com.xdrapor.safeguard.core.logging.SGLogManager;
import com.xdrapor.safeguard.core.permissions.SGPermissibles;
import com.xdrapor.safeguard.player.SGPlayerManager;

public class SafeGuard extends JavaPlugin implements ICore
{
	//TODO: Delete this
	public SGLoadManager sgLoadManager;

	/** A reference to the static instance of SGLogManager. */
	public SGLogManager sgLogManager;

	/** A reference to the static instance of SGPermissions. */
	public SGPermissibles sgPermissions;

	/** A reference to the static instance of SGConfig. */
	public SGConfig sgConfig;

	/** A reference to the static instance of SGPlayerManager. */
	public SGPlayerManager sgPlayerManager;

	/**
	 * Executed when SafeGuard is enabled.
	 * @see org.bukkit.plugin.java.JavaPlugin#onDisable()
	 */
	@Override
	public void onEnable() {

		// Construct an instance of SGLoadManager.
		sgLoadManager	= new SGLoadManager();

		// Reference instances.
		sgLogManager	= sgLoadManager.getLogManagerInstance();
		sgPermissions	= sgLoadManager.getPermissionsInstance();
		sgConfig		= sgLoadManager.getConfigInstance();
		sgPlayerManager = sgLoadManager.getPlayerManagerInstance();

		// Initialize plugin.
		sgLoadManager.loadListeners();
		sgLoadManager.registerListeners();
		sgPermissions.setupPermissions();
		sgConfig.saveConfig();

		// Log completion to console.
		sgLogManager.getConsoleLogger().logInfo(sgPrefix + sgStringSeparator + "has been enabled.");
	}

	/**
	 * Executed when SafeGuard is disabled.
	 * @see org.bukkit.plugin.java.JavaPlugin#onEnable()
	 */
	@Override
	public void onDisable() {

		sgLoadManager.cleanUpListeners();
		sgLogManager.fileHandler.close();
		sgLogManager.getConsoleLogger().logInfo(sgPrefix + sgStringSeparator + "has been disabled!");
	}
}
