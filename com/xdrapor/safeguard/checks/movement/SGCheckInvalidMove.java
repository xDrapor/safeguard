package com.xdrapor.safeguard.checks.movement;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerMoveEvent;

import com.xdrapor.safeguard.checks.SGCheck;
import com.xdrapor.safeguard.core.permissions.SGPermissibleNodes;
import com.xdrapor.safeguard.player.SGPlayer;
import com.xdrapor.safeguard.utilities.SGCheckTag;
import com.xdrapor.safeguard.utilities.SGMovementUtil;

public class SGCheckInvalidMove extends SGCheck{

	@Override
	public String getDescription() {
		return "Disallows invalid/illegal moves. (Sprinting backwards, standing still and sprinting, etc)";
	}

	@Override
	public void runCheck(Event event, SGPlayer player) {
		
		PlayerMoveEvent playerMoveEvent = (PlayerMoveEvent)event;
		Player sgPlayer = player.getPlayer();
		
		this.to = playerMoveEvent.getTo();
		this.from = playerMoveEvent.getFrom();
		
		if(sgPermissions.hasPermission(player, SGPermissibleNodes.MOVEMENT_SPEED) || !sgConfig.isCheckEnabled(this))return;
		
		if(isSprinting(sgPlayer))
		{
			
			// TODO: Add this back in, was in a rush and it was low on priority list. Look at the methods in SGMovementData to see if it can be written with some.
			/*
			if(SGMovementData.isBackPedalCheat())
			{
				
				safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).addVL(SGCheckTag.MOVEMENT_INVALID, 20.0D);
				//sgCheckThrown.addVL(sgPlayerMovementData.getBackPedalDiff() - player.getEPlayer().pitch);
				publishCheck(sgPlayer, SGCheckTag.MOVEMENT_INVALID);
				
				playerMoveEvent.setTo(this.from);

				return;
			}
			*/
			if(SGMovementUtil.getDistanceHorizontal(this.to, this.from) == 0.0)
			{
				
				safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).addVL(SGCheckTag.MOVEMENT_INVALID, 20.0D);

				publishCheck(sgPlayer, SGCheckTag.MOVEMENT_INVALID);
				
				playerMoveEvent.setTo(this.from);

				return;
			}
		}
	}
}