package com.xdrapor.safeguard.checks.movement;

import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerMoveEvent;

import com.xdrapor.safeguard.checks.SGCheck;
import com.xdrapor.safeguard.core.permissions.SGPermissibleNodes;
import com.xdrapor.safeguard.player.SGPlayer;
import com.xdrapor.safeguard.utilities.SGCheckTag;
import com.xdrapor.safeguard.utilities.SGMovementUtil;

public class SGCheckFlight extends SGCheck 
{

	@Override
	public String getDescription() {
		return "Prevents unauthorized flight";
	}

	@Override
	public void runCheck(Event event, SGPlayer player) 
	{
		PlayerMoveEvent playerMoveEvent = (PlayerMoveEvent)event;
		Player sgPlayer = player.getPlayer();
		
		this.to = playerMoveEvent.getTo();
		this.from = playerMoveEvent.getFrom();
		
		if(sgPermissions.hasPermission(player, SGPermissibleNodes.MOVEMENT_FLIGHT) || !sgConfig.isCheckEnabled(this))return;

		if(onGround(sgPlayer) || inLiquid(sgPlayer))
		{
			SGMovementUtil.setSafeLocation(sgPlayer);
			
			// TODO: Implement new VL system
			
			//safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).resetVL(SGCheckTag.MOVEMENT_DISTANCE);
			//safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).resetVL(SGCheckTag.MOVEMENT_VERTICAL);
			
			return;
		}

		if(isCreative(sgPlayer))
		{
			//TODO: Perform creative mode checks.
			
			//I'll handle it -Richard
			return;
		}
		
		if(SGMovementUtil.getSafeFalling(sgPlayer) && SGMovementUtil.getSafeDistanceX(sgPlayer, true) < 6 && SGMovementUtil.getSafeDistanceZ(sgPlayer, true) < 6 || SGMovementUtil.getSafeDistanceVertical(sgPlayer) < ((double)getJumpAmplifier(((CraftPlayer)sgPlayer).getHandle()) * 1.25) && SGMovementUtil.getSafeDistanceHorizontal(sgPlayer) < 5)
		{

			// TODO: Implement new VL system
			
			//safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).resetVL(SGCheckTag.MOVEMENT_DISTANCE);
			//safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).resetVL(SGCheckTag.MOVEMENT_VERTICAL);
			
			return;
		}

		if(SGMovementUtil.getSafeDistanceHorizontal(sgPlayer) > 0.6 && (SGMovementUtil.getSafeDistanceVertical(sgPlayer)) > 0.6)	
		{
			safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).addVL(SGCheckTag.MOVEMENT_DISTANCE, SGMovementUtil.getSafeDistanceHorizontal(sgPlayer) + SGMovementUtil.getSafeDistanceVertical(sgPlayer));
			safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).addVL(SGCheckTag.MOVEMENT_VERTICAL, SGMovementUtil.getSafeDistanceHorizontal(sgPlayer) + SGMovementUtil.getSafeDistanceVertical(sgPlayer));
			
			publishCheck(sgPlayer, SGCheckTag.MOVEMENT_DISTANCE);
			publishCheck(sgPlayer, SGCheckTag.MOVEMENT_VERTICAL);
			
			playerMoveEvent.setTo(SGMovementUtil.getSafeLocation(sgPlayer));
			
			return;
		}
		else if(SGMovementUtil.getSafeDistanceHorizontal(sgPlayer) > 0.6)	
		{
			safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).addVL(SGCheckTag.MOVEMENT_DISTANCE, SGMovementUtil.getSafeDistanceHorizontal(sgPlayer));

			publishCheck(sgPlayer, SGCheckTag.MOVEMENT_DISTANCE);
			
			playerMoveEvent.setTo(SGMovementUtil.getSafeLocation(sgPlayer));
			
			return;
		}
		else if((SGMovementUtil.getSafeDistanceVertical(sgPlayer)) > 0.6)
		{
			safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).addVL(SGCheckTag.MOVEMENT_VERTICAL, SGMovementUtil.getSafeDistanceVertical(sgPlayer));

			publishCheck(sgPlayer, SGCheckTag.MOVEMENT_VERTICAL);
			
			playerMoveEvent.setTo(SGMovementUtil.getSafeLocation(sgPlayer));
			
			return;
		}
	}
}