package com.xdrapor.safeguard.checks.combat;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.xdrapor.safeguard.checks.SGCheck;
import com.xdrapor.safeguard.core.permissions.SGPermissibleNodes;
import com.xdrapor.safeguard.player.SGPlayer;
import com.xdrapor.safeguard.utilities.SGCheckTag;

public class SGCheckSpeed extends SGCheck
{

	@Override
	public String getDescription()
	{
		return "Prevents a player from attacking an entity too quickly.";
	}

    @Override
    public void runCheck(Event event, SGPlayer player)
    {
		EntityDamageByEntityEvent eDBeEvent = (EntityDamageByEntityEvent)event;
    	Player sgPlayer = player.getPlayer();
    	
    	if(sgPermissions.hasPermission(player, SGPermissibleNodes.COMBAT_SPEED) || !sgConfig.isCheckEnabled(this))return;
    	
    	/* TODO: xDrapor you'll have to adjust this shit cause it's late, I'm tired, and I believe you know better what to do.

    	if (eDBeEvent.getDamager().equals(sgPlayer)) {
    		if (System.currentTimeMillis() - safeGuard.sgPlayerManager.getPlayer(sgPlayer.getPlayer().getName()).getLastHitTime(................)) {
    			
    			safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).addVL(SGCheckTag.COMBAT_SPEED, System.currentTimeMillis() - 0);

    			if (safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).getVL(SGCheckTag.COMBAT_SPEED) > this.maxBuffer) {
    				
    				publishCheck(sgPlayer, SGCheckTag.COMBAT_SPEED);
    				
    				eDBeEvent.setDamage(0);
    				eDBeEvent.setCancelled(true);
    				
    				return;
    			}
    			
    		} else {
    			
    			safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).resetVL(SGCheckTag.COMBAT_SPEED);
    			return;
    		}
    	}

		
       /* ORIGINAL:
        if(playerData.isAttackerPlayer())
        {
        	
                if(System.currentTimeMillis() - playerData.getLastHit() < 126)
                {
                	sgCheckThrown.addVL(System.currentTimeMillis() - playerData.getLastHit());
                        
                        sgCheckThrown.setTags(SGCheckTag.COMBAT_SPEED.toString());
                        
                        if(sgCheckThrown.getVL() > maxBuffer)
                        {
                        	sgCheckThrown.publishCheck();
                                ((EntityDamageByEntityEvent)evt).setDamage(0);
                                ((EntityDamageByEntityEvent)evt).setCancelled(true);
                                return;
                        }
                }
                else
                {
                        playerData.setLastHit();
                        sgCheckThrown.remVL();
                        return;
                }

        }
       */
    }
}