package com.xdrapor.safeguard.core.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Handles commands for SafeGuard.
 * 
 * @author xDrapor
 *
 */
public class SGCommandManager extends SGCommand {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) 
	{
		return false;
	}

	@Override
	public String getName() 
	{
		return sgPrefix.toLowerCase();
	}

}
