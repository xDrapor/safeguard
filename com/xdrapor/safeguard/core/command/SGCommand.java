package com.xdrapor.safeguard.core.command;

import org.bukkit.command.CommandExecutor;

import com.xdrapor.safeguard.core.ICore;
import com.xdrapor.safeguard.core.configuration.SGConfig;
import com.xdrapor.safeguard.core.permissions.SGPermissibles;

public abstract class SGCommand implements CommandExecutor, ICore 
{

	protected SGPermissibles sgPermissions = new SGPermissibles();
	protected SGConfig sgConfig = new SGConfig();
	
	public boolean enoughArguments(int argLength, int... dimensions)
	{
		if(dimensions.length > 1)
		{
			return argLength > dimensions[0] && argLength < dimensions[1];
		}
		return argLength > dimensions[0];
	}
	
	public abstract String getName();
}
