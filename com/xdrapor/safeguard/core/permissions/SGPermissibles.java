package com.xdrapor.safeguard.core.permissions;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.xdrapor.safeguard.player.SGPlayer;

/**
 * Handles permissions for SafeGuard.
 * 
 * @author xDrapor
 * @author IchBinMude - I changed your return on hasPermission :D (so much cleaner).
 *
 */
public class SGPermissibles {
	
	/** The static instance of permission provider. */
	private static Permission permissionHandler = null;

	/** Setup permissions for SafeGuard. */
	public boolean setupPermissions() {
		
		RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		
		if (permissionProvider != null) {
			permissionHandler = permissionProvider.getProvider();
		}
		
		return (permissionHandler != null);
	}

	/** Returns whether a player has permission to bypass a check. */
	public boolean hasPermission(SGPlayer sgPlayer, String permission) {
		return isPermissionHandlerSetup() ? (permissionHandler.has(sgPlayer.getPlayer(), permission) || sgPlayer.getPlayer().isOp() ? true : false) : false;
	}

	/** Returns whether the permission handler has been setup. */
	public boolean isPermissionHandlerSetup() {
		return permissionHandler != null;
	}
}
