package com.xdrapor.safeguard.core.permissions;

/**
 * Permissible Nodes.
 * 
 * @author xDrapor
 *
 */
public class SGPermissibleNodes {

	public static String MOVEMENT_FLIGHT = "safeguard.movement.flight";
	public static String MOVEMENT_SPEED = "safeguard.movement.speed";
	public static String MOVEMENT_INVALID = "safeguard.combat.speed";
	
	public static String BLOCK_REACH = "safeguard.block.reach";
	public static String BLOCK_SPEED = "safeguard.block.speed";
	
	public static String COMBAT_SPEED = "safeguard.combat.speed";
	
	public static String INFO_ALERTS = "safeguard.info.alerts";
	
}
