package com.xdrapor.safeguard.core.loading;

import org.bukkit.Bukkit;

import com.xdrapor.safeguard.core.ICore;
import com.xdrapor.safeguard.core.command.SGCommand;
import com.xdrapor.safeguard.core.command.SGCommandManager;
import com.xdrapor.safeguard.core.configuration.SGConfig;
import com.xdrapor.safeguard.core.logging.SGLogManager;
import com.xdrapor.safeguard.core.permissions.SGPermissibles;
import com.xdrapor.safeguard.event.SGEventListener;
import com.xdrapor.safeguard.event.blockbreak.SGEventBlockBreak;
import com.xdrapor.safeguard.event.combat.SGEventCombat;
import com.xdrapor.safeguard.event.movement.SGEventMovement;
import com.xdrapor.safeguard.event.server.SGEventPlayerStatus;
import com.xdrapor.safeguard.player.SGPlayerManager;

public class SGLoadManager implements ICore
{

	/** The static instance of SGLogManager. */
	public static SGLogManager sgLogManager;

	/** The static instance of SGPermissions. */
	public static SGPermissibles sgPermissions;

	/** The static instance of SGConfig. */
	public static SGConfig sgConfig;

	/** The static instance of SGPlayerManager. */
	public static SGPlayerManager sgPlayerManager;
	
	/** Construct a new SGLoadManager. */
	public SGLoadManager() 
	{

		sgLogManager	= new SGLogManager();
		sgPermissions	= new SGPermissibles();
		sgConfig		= new SGConfig();
		sgPlayerManager = new SGPlayerManager();
		
	}

	/** Returns the SGLogManager instance. */
	public final SGLogManager getLogManagerInstance() 
	{
		return sgLogManager;
	}

	/** Returns the SGPermissibles instance. */
	public final SGPermissibles getPermissionsInstance() 
	{
		return sgPermissions;
	}

	/** Returns the SGConfig instance. */
	public final SGConfig getConfigInstance()
	{
		return sgConfig;
	}
	
	/** Returns the SGPlayerManager instance. */
	public final SGPlayerManager getPlayerManagerInstance() {
		return sgPlayerManager;
	}

	public void loadListeners()
	{
		systemListeners.add(new SGEventPlayerStatus());
		systemListeners.add(new SGEventMovement());
		systemListeners.add(new SGEventBlockBreak());
		systemListeners.add(new SGEventCombat());
	}

	public void loadCommands()
	{
		systemCommands.add(new SGCommandManager());
	}

	public void registerListeners()
	{
		for(SGEventListener listener : systemListeners)
		{
			listener.loadChecks();
			Bukkit.getPluginManager().registerEvents(listener, Bukkit.getPluginManager().getPlugin("SafeGuard"));
		}
	}

	public void cleanUpListeners()
	{
		systemListeners.clear();
	}

	public void registerCommands()
	{
		for(SGCommand command : systemCommands)
		{
			safeGuard.getCommand(command.getName()).setExecutor(command);
		}
	}
}
