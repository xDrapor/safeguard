package com.xdrapor.safeguard.utilities;

import net.minecraft.server.Item;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.xdrapor.safeguard.core.ICore;
import com.xdrapor.safeguard.player.SGPlayer;

/**
 * This is a static class that will not be instanced ever. The 'data' is already instanced in the Event we are passed, no reason to create it again. <-- Rename once understood.
 * 
 * @author xDrapor
 * @author IchBinMude
 * @author Richard
 * 
 */
public class SGBlockUtil implements ICore
{

	// TODO: Rename this method as it's too vague.
	/** Returns the time between the last block break and last block hit times of the specified SGPlayer. */
	public static double getDuration(Player sgPlayer) {
		return Math.round(safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).getLastBlockBreakTime() - safeGuard.sgPlayerManager.getPlayer(sgPlayer.getName()).getLastBlockHitTime());
	}
	
	/** Returns a block hardness value. */
	public static double getHardness(Block block)
	{
		return net.minecraft.server.Block.byId[block.getTypeId()].m(((CraftWorld)block.getWorld()).getHandle(), block.getLocation().getBlockX(), block.getLocation().getBlockY(), block.getLocation().getBlockZ());
	}

	/** Returns how long it should take to break the block with the specified tool. */
	public static double getDurationVSTool(SGPlayer player, ItemStack item, Block block)
	{
		float currentToolMultiplier = item.getTypeId() == 0 ? 1.0F : net.minecraft.server.Item.byId[item.getTypeId()].getDestroySpeed(new net.minecraft.server.ItemStack(Item.byId[item.getTypeId()]), net.minecraft.server.Block.byId[block.getTypeId()]);
		double currentBlockHardness = getHardness(block);
		return Math.round((1000f * 5f * currentBlockHardness) / (currentToolMultiplier * 3.33f));
	}
	
	/** Returns the closest ground Location relative to the specified Location. */
	public static Location findClosestGroundToLocation(Location location)
	{
		for(int i = 0;; i++)
		{
			if(!(location.getWorld().getBlockAt(location.subtract(0, i, 0)).isEmpty()))
			{
				return location.add(0, 1, 0);
			}
		}
	}
	
	/** Returns true if the specified Block is a fence. */
	public static boolean isFence(final Block block)
	{
		return block.getType() == Material.NETHER_FENCE || block.getType() == Material.FENCE;
	}

	/** Returns true if the specified Block is a stair. */
	public static boolean isStair(final Block block)
	{
		return block.getType() == Material.WOOD_STEP
				|| block.getType() == Material.STEP
				|| block.getType() == Material.BRICK_STAIRS
				|| block.getType() == Material.COBBLESTONE_STAIRS
				|| block.getType() == Material.WOOD_STAIRS
				|| block.getType() == Material.BIRCH_WOOD_STAIRS
				|| block.getType() == Material.JUNGLE_WOOD_STAIRS
				|| block.getType() == Material.SPRUCE_WOOD_STAIRS
				|| block.getType() == Material.NETHER_BRICK_STAIRS
				|| block.getType() == Material.SANDSTONE_STAIRS
				|| block.getType() == Material.SMOOTH_STAIRS
				|| block.getType() == Material.WOOD_DOUBLE_STEP;
	}
	
	/** Returns true if the specified Block is snow. */
	public static boolean isSnow(final Block block)
	{
		return block.getType() == Material.SNOW;
	}
}