package com.xdrapor.safeguard.player;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.xdrapor.safeguard.utilities.SGCheckTag;

/**
 * Represents a player currently tracked by SafeGuard.
 * 
 * @author IchBinMude
 *
 */
public class SGPlayer {

	/** Represents the players UserName. */
	private final String sgPlayerName;
	
	/** Represents the current violation levels of all SGCheckTags for this player. */
	private Map<SGCheckTag, Double> violations = new HashMap<SGCheckTag, Double>();
	
	/** Represents the attackers of this player and the last time they attacked. */
	private Map<String, Long> lastHitTime = new HashMap<String, Long>();
	
	/** The last safe location of the player. */
	private Location safeLocation = null;

	/** The last time this player hit a block. */
	private long lastBlockHitTime = 0L;
	
	/** The last time this player broke a block. */
	private long lastBlockBreakTime = 0L;
	
	/** Construct a new SGPlayer instance. */
	public SGPlayer(String playerName) {
		
		this.sgPlayerName = playerName;
		
		for (SGCheckTag tag : SGCheckTag.values()) {
			this.violations.put(tag, 0.0D);
		}
	}
	
	/** Returns the player instance. */
	public Player getPlayer() {
		return Bukkit.getPlayer(this.sgPlayerName);
	}
	
	/** Returns the current safe location of the player. */
	public Location getSafeLocation() {
		return (this.safeLocation);
	}
	
	/** Sets the current safe location of the player. */
	public void setSafeLocation(Location safeLocation) {
		this.safeLocation = safeLocation;
	}
	
	/** Resets the current safe location of the player. */
	public void resetSafeLocation() {
		this.safeLocation = null;
	}
	
	/** Returns the time in milliseconds the player was last hit by the specified attacker. */
	public long getLastHitTime(String attacker) {
		return this.lastHitTime.get(attacker).longValue();
	}
	
	/** Sets the time in milliseconds the player was last hit by the specified attacker. */
	public void setLastHitTime(String attacker, long lastHitTime) {
		this.lastHitTime.put(attacker, lastHitTime);
	}
	
	/** Returns the last time this player hit a block. */
	public long getLastBlockHitTime() {
		return (this.lastBlockHitTime);
	}
	
	/** Sets the last time this player hit a block. */
	public void setLastBlockHitTime(long lastBlockHitTime) {
		this.lastBlockHitTime = lastBlockHitTime;
	}
	
	/** Returns the last time this player broke a block. */
	public long getLastBlockBreakTime() {
		return (this.lastBlockBreakTime);
	}
	
	/** Sets the last time this player broke a block. */
	public void setLastBlockBreakTime(long lastBlockBreakTime) {
		this.lastBlockBreakTime = lastBlockBreakTime;
	}
	
	/** Returns the violation level of the player for the specified tag. */
	public double getVL(SGCheckTag tag) {
		return (this.violations.get(tag).doubleValue());
	}

	/** Sets the violation level of the player for the specified tag. */
	public void setVL(SGCheckTag tag, double value) {
		this.violations.put(tag, value);
	}
	
	/** Adds to the violation level of the player for the specified tag. */
	public void addVL(SGCheckTag tag, double value) {
		double oldValue = this.getVL(tag);
		this.setVL(tag, oldValue + value);
	}
	
	/** Returns the violation level of the player for the specified tag truncated to a max of two decimal places. */
	public double getVLTruncated(SGCheckTag tag) {
		return (Double.parseDouble(new DecimalFormat("#.##").format(this.violations.get(tag).doubleValue())));
	}
	
	/** Resets the specified player violation level. */
	public void resetVL(SGCheckTag tag) {
		this.violations.put(tag, 0.0D);
	}

	/** Resets ALL the player violation levels. */
	public void resetAllVL() {
		
		this.violations.clear();

		for (SGCheckTag tag : SGCheckTag.values()) {
			this.violations.put(tag, 0.0D);
		}		
	}
}