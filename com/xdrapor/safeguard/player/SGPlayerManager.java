package com.xdrapor.safeguard.player;

import java.util.HashMap;
import java.util.Map;

import com.xdrapor.safeguard.core.ICore;

public class SGPlayerManager implements ICore {

	/** A map of player UserNames and their SGPlayer counterpart. */
	public Map<String, SGPlayer> sgPlayerData = new HashMap<String, SGPlayer>();
	
	/** Construct a new SGPlayerManager instance. */
	public SGPlayerManager() {
		
	}
	
	/** Adds an SGPlayer instance for SafeGuard to track. */
	public void addPlayer(String username, SGPlayer sgPlayer) {
		this.sgPlayerData.put(username, sgPlayer);
	}
	
	/** Removes an SGPlayer instance SafeGuard is tracking. */
	public void removePlayer(String username) {
		this.sgPlayerData.remove(username);
	}
	
	/** Returns the SGPlayer instance for the specified UserName. */
	public SGPlayer getPlayer(String username) {
		return (this.sgPlayerData.get(username));
	}

	/** Returns a map of the current SGPlayer instances SafeGuard is tracking. */
	public Map<String, SGPlayer> getPlayers() {
		return (this.sgPlayerData);
	}
}
